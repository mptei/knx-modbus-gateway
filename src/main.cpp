#include <knx.h>
#include <Wire.h>
#include "hardware.h"
#include "Device.h"

void appSetup();
void appLoop();

void setup()
{

    SERIAL_PORT.begin(115200);
    pinMode(get_PROG_LED_PIN(get_HW_ID()), OUTPUT);
    digitalWrite(get_PROG_LED_PIN(get_HW_ID()), HIGH);
    //delay(5000); // ******************  ENTFERNEN   ********************************************
    digitalWrite(get_PROG_LED_PIN(get_HW_ID()), LOW);
    SERIAL_PORT.println("Startup called...");
    ArduinoPlatform::SerialDebug = &SERIAL_PORT;

    //I2C Init
    Wire.begin();

    initHW(get_HW_ID());

    knx.readMemory();

    // pin or GPIO the programming led is connected to. Default is LED_BUILDIN
    knx.ledPin(get_PROG_LED_PIN(get_HW_ID()));
    // is the led active on HIGH or low? Default is LOW
    knx.ledPinActiveOn(PROG_LED_PIN_ACTIVE_ON);
    // pin or GPIO programming button is connected to. Default is 0
    knx.buttonPin(get_PROG_BUTTON_PIN(get_HW_ID()));
    // Is the interrup created in RISING or FALLING signal? Default is RISING
    knx.buttonPinInterruptOn(PROG_BUTTON_PIN_INTERRUPT_ON);

    // print values of parameters if device is already configured
    if (knx.configured())
        appSetup();

    // start the framework.
    knx.start();
}

void loop()
{
    // don't delay here to much. Otherwise you might lose packages or mess up the timing with ETS
    knx.loop();

    // only run the application code if the device was configured with ETS
    if (knx.configured())
    {
        appLoop();
    }
}