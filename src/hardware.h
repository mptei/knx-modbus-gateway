#pragma once


#define ArduinoZero
//#define ItsyBitsyM0

#ifdef ArduinoZero
#define SERIAL_PORT SerialUSB   // Serial port for Arduino Zero
#endif 
#ifdef ItsyBitsyM0
//#define SERIAL_PORT Serial      // Serial port for ItsyBitsy
#endif

// KNX 
#define PROG_LED_PIN_ACTIVE_ON HIGH
#define PROG_BUTTON_PIN_INTERRUPT_ON FALLING

//DIAG LED
#define Diag_LED 9

//Gehäuse Anzeige
#define  i2cAddr_LED   0x24

//HW-ID
#define ID1 23
#define ID2 24
#define ID3 22

// Modbus 
#define ModbusChannel 100    
#define MaxCountSlaves 10
/*!
  We're using a MAX485-compatible RS485 Transceiver.
  The Data Enable and Receiver Enable pins are hooked up as follows:
*/
#define MAX485_DE 12


// S0
#define S0_CH1 2
#define S0_CH2 5

// Virtuelle Zähler
#define maxMeters 4





