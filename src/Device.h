#pragma once


void ProcessHeartbeat();

uint8_t get_HW_ID();
void initHW(uint8_t hwID);
uint8_t get_PROG_LED_PIN(uint8_t hwID);
uint8_t get_PROG_BUTTON_PIN(uint8_t hwID);
