#pragma once
#include <knx.h>

// Parameter with single occurance
#define MOD_StartupDelay               0      // int32_t
#define MOD_StartupDelaySelection      4      // char*, 2 Byte
#define MOD_Heartbeat                  6      // int32_t
#define MOD_Res_P4                    10      // int8_t
#define MOD_Res_P5                    11      // int8_t
#define MOD_Res_P6                    12      // int8_t
#define MOD_Res_P7                    13      // int8_t
#define MOD_Res_P8                    14      // int8_t
#define MOD_Res_P9                    15      // int8_t
#define MOD_BusBaudrateSelection      16      // 8 Bits, Bit 7-0
#define MOD_BusParitySelection        17      // 8 Bits, Bit 7-0
#define MOD_BusDelayRequest           18      // 8 Bits, Bit 7-0
#define MOD_BusDelayCycle             19      // 8 Bits, Bit 7-0
#define MOD_BusID_Slave1              20      // int32_t
#define MOD_BusByteOrderSelectionSlave1 24      // 8 Bits, Bit 7-0
#define MOD_BusWordOrderSelectionSlave1 25      // 8 Bits, Bit 7-0
#define MOD_BusID_Slave2              26      // int32_t
#define MOD_BusByteOrderSelectionSlave2 30      // 8 Bits, Bit 7-0
#define MOD_BusWordOrderSelectionSlave2 31      // 8 Bits, Bit 7-0
#define MOD_BusID_Slave3              32      // int32_t
#define MOD_BusByteOrderSelectionSlave3 36      // 8 Bits, Bit 7-0
#define MOD_BusWordOrderSelectionSlave3 37      // 8 Bits, Bit 7-0
#define MOD_BusID_Slave4              38      // int32_t
#define MOD_BusByteOrderSelectionSlave4 42      // 8 Bits, Bit 7-0
#define MOD_BusWordOrderSelectionSlave4 43      // 8 Bits, Bit 7-0
#define MOD_BusID_Slave5              44      // int32_t
#define MOD_BusByteOrderSelectionSlave5 48      // 8 Bits, Bit 7-0
#define MOD_BusWordOrderSelectionSlave5 49      // 8 Bits, Bit 7-0
#define MOD_BusID_Slave6              50      // int32_t
#define MOD_BusByteOrderSelectionSlave6 54      // 8 Bits, Bit 7-0
#define MOD_BusWordOrderSelectionSlave6 55      // 8 Bits, Bit 7-0
#define MOD_BusID_Slave7              56      // int32_t
#define MOD_BusByteOrderSelectionSlave7 60      // 8 Bits, Bit 7-0
#define MOD_BusWordOrderSelectionSlave7 61      // 8 Bits, Bit 7-0
#define MOD_BusID_Slave8              62      // int32_t
#define MOD_BusByteOrderSelectionSlave8 66      // 8 Bits, Bit 7-0
#define MOD_BusWordOrderSelectionSlave8 67      // 8 Bits, Bit 7-0
#define MOD_BusID_Slave9              68      // int32_t
#define MOD_BusByteOrderSelectionSlave9 72      // 8 Bits, Bit 7-0
#define MOD_BusWordOrderSelectionSlave9 73      // 8 Bits, Bit 7-0
#define MOD_BusID_Slave10             74      // int32_t
#define MOD_BusByteOrderSelectionSlave10 78      // 8 Bits, Bit 7-0
#define MOD_BusWordOrderSelectionSlave10 79      // 8 Bits, Bit 7-0
#define MOD_MBusBaurateSelection      80      // 8 Bits, Bit 7-0
#define MOD_MBusParitySelection       81      // 8 Bits, Bit 7-0
#define MOD_Res_P46                   82      // int8_t
#define MOD_Res_P47                   83      // int8_t
#define MOD_Res_P48                   84      // int8_t
#define MOD_Res_P49                   85      // int8_t
#define MOD_Res_P50                   86      // int8_t
#define MOD_Res_P51                   87      // int8_t
#define MOD_Res_P52                   88      // int8_t
#define MOD_Res_P53                   89      // int8_t
#define MOD_Res_P54                   90      // int8_t
#define MOD_Res_P55                   91      // int8_t
#define MOD_S01Impulse                92      // uint16_t
#define MOD_S02Impulse                94      // uint16_t
#define MOD_DefineS0zaehler1          96      // 8 Bits, Bit 7-0
#define MOD_DefineS0zaehler2          97      // 8 Bits, Bit 7-0
#define MOD_SendDelayS01              98      // int32_t
#define MOD_SendDelayS02              102      // int32_t
#define MOD_defineStartCounterS01     106      // 1 Bit, Bit 7
#define     MOD_defineStartCounterS01Mask 0x80
#define     MOD_defineStartCounterS01Shift 7
#define MOD_defineStartCounterS02     107      // 1 Bit, Bit 7
#define     MOD_defineStartCounterS02Mask 0x80
#define     MOD_defineStartCounterS02Shift 7
#define MOD_Modbus                    115      // 1 Bit, Bit 7
#define     MOD_ModbusMask 0x80
#define     MOD_ModbusShift 7
#define MOD_Mbus                      116      // 1 Bit, Bit 6
#define     MOD_MbusMask 0x40
#define     MOD_MbusShift 6
#define MOD_S0                        117      // 1 Bit, Bit 5
#define     MOD_S0Mask 0x20
#define     MOD_S0Shift 5
#define MOD_ModBusZaehler1SendDelay   118      // int32_t
#define MOD_ModBusZaehler1ValueChangeWatt 121      // int32_t
#define MOD_ModbusZaehler1SendenWertAenderung 125      // 1 Bit, Bit 6
#define     MOD_ModbusZaehler1SendenWertAenderungMask 0x40
#define     MOD_ModbusZaehler1SendenWertAenderungShift 6
#define MOD_ModbusZaehler1SendenWertCyclisch 125      // 1 Bit, Bit 5
#define     MOD_ModbusZaehler1SendenWertCyclischMask 0x20
#define     MOD_ModbusZaehler1SendenWertCyclischShift 5
#define MOD_ModbusZaehle1rUeberschuss 125      // 1 Bit, Bit 4
#define     MOD_ModbusZaehle1rUeberschussMask 0x10
#define     MOD_ModbusZaehle1rUeberschussShift 4
#define MOD_ModbusZaehler1selection   125      // 1 Bit, Bit 7
#define     MOD_ModbusZaehler1selectionMask 0x80
#define     MOD_ModbusZaehler1selectionShift 7
#define MOD_ModbusZaehler1Offset      126      // int32_t
#define MOD_ModbusZaehler1DPT         130      // 8 Bits, Bit 7-0
#define MOD_S01CalDef                 134      // 8 Bits, Bit 7-0
#define MOD_S02CalDef                 135      // 8 Bits, Bit 7-0
#define MOD_S01SendModeCounter        136      // 8 Bits, Bit 7-0
#define MOD_S02SendModeCounter        137      // 8 Bits, Bit 7-0
#define MOD_SendminValueDelayS01      138      // uint16_t
#define MOD_SendminValueDelayS02      140      // uint16_t
#define MOD_SendminValuechangeS01     144      // uint16_t
#define MOD_SendminValuechangeS02     146      // uint16_t
#define MOD_SendDelayConS01           148      // int32_t
#define MOD_SendDelayConS02           152      // int32_t
#define MOD_SendminValuechangeConS01  156      // uint16_t
#define MOD_SendminValuechangeConS02  158      // uint16_t
#define MOD_S01SendModeCon            160      // 8 Bits, Bit 7-0
#define MOD_S02SendModeCon            161      // 8 Bits, Bit 7-0
#define MOD_SendminValueDelayConS01   162      // uint16_t
#define MOD_SendminValueDelayConS02   164      // uint16_t
#define MOD_DefineUnitS01             166      // 8 Bits, Bit 7-0
#define MOD_DefineUnitS02             167      // 8 Bits, Bit 7-0
#define MOD_DefineMinValueS01         168      // uint16_t
#define MOD_DefineMinValueS02         170      // uint16_t

// Parameter per channel
#define MOD_ParamBlockOffset 172
#define MOD_ParamBlockSize 38
#define MOD_CHModbusSlaveSelection     0      // 8 Bits, Bit 7-0
#define MOD_CHModBusDptSelection       1      // 8 Bits, Bit 7-0
#define MOD_CHModBusBusDirection       2      // 8 Bits, Bit 7-0
#define MOD_CHModbusRegister           3      // int32_t
#define MOD_CHModBusSendDelay          7      // int32_t
#define MOD_CHModBusValueChange       11      // int32_t
#define MOD_CHModBusReadCycle         15      // 8 Bits, Bit 7-0
#define MOD_CHModBusInputTypDpt1      16      // 8 Bits, Bit 7-0
#define MOD_CHModBusRegisterPosDPT5001 16      // 8 Bits, Bit 7-0
#define MOD_CHModBusRegisterPosDPT5   16      // 8 Bits, Bit 7-0
#define MOD_CHModBusReserve4          16      // int8_t
#define MOD_CHModBusRegisterPosDPT7   16      // 8 Bits, Bit 7-0
#define MOD_CHModBusReserve6          16      // int8_t
#define MOD_CHModBusRegisterPosDPT9   16      // 8 Bits, Bit 7-0
#define MOD_CHModBusRegisterPosDPT12  16      // 8 Bits, Bit 7-0
#define MOD_CHModBusRegisterPosDPT13  16      // 8 Bits, Bit 7-0
#define MOD_CHModBusRegisterPosDPT14  16      // 8 Bits, Bit 7-0
#define MOD_CHModBusInputTypInvDpt1   17      // 8 Bits, Bit 7-0
#define MOD_CHModBusReserve12         17      // int8_t
#define MOD_CHModbusCountBitsDPT56    17      // 8 Bits, Bit 7-0
#define MOD_CHModBusReserve14         17      // int8_t
#define MOD_CHModbusCountBitsDPT7     17      // 8 Bits, Bit 7-0
#define MOD_CHModBusReserve16         17      // int8_t
#define MOD_CHModBusReserve17         17      // int8_t
#define MOD_CHModBusWordPosDpt12      17      // 8 Bits, Bit 7-0
#define MOD_CHModBusWordPosDpt13      17      // 8 Bits, Bit 7-0
#define MOD_CHModBusWordPosDpt14      17      // 8 Bits, Bit 7-0
#define MOD_CHModBusBitPosDpt1        18      // 8 Bits, Bit 7-0
#define MOD_CHModBusReserve22         18      // 8 Bits, Bit 7-0
#define MOD_CHModBusOffsetRight5      18      // 8 Bits, Bit 7-0
#define MOD_CHModBusReserve24         18      // int8_t
#define MOD_CHModBusOffsetRight7      18      // 8 Bits, Bit 7-0
#define MOD_CHModBusReserve26         18      // int8_t
#define MOD_CHModBusReserve27         18      // int8_t
#define MOD_CHModBusWordTyp12         18      // 8 Bits, Bit 7-0
#define MOD_CHModBusWordTyp13         18      // 8 Bits, Bit 7-0
#define MOD_CHModBusWordTyp14         18      // 8 Bits, Bit 7-0
#define MOD_CHModBusReserve31         19      // int8_t
#define MOD_CHModBusReserve32         19      // int8_t
#define MOD_CHModBusReserve33         19      // int8_t
#define MOD_CHModBusReserve34         19      // int8_t
#define MOD_CHModBusReserve35         19      // int8_t
#define MOD_CHModBusReserve36         19      // int8_t
#define MOD_CHModBusReserve37         19      // int8_t
#define MOD_CHModBusReserve38         19      // int8_t
#define MOD_CHModBusReserve39         19      // int8_t
#define MOD_CHModBusRegisterValueTypDpt14 19      // 8 Bits, Bit 7-0
#define MOD_CHModBusReadBitFunktion   20      // 8 Bits, Bit 7-0
#define MOD_CHModBusReadWordFunktion  20      // 8 Bits, Bit 7-0
#define MOD_CHModBusWriteWordFunktion 20      // 8 Bits, Bit 7-0
#define MOD_CHModBusReserve44         20      // int8_t
#define MOD_CHModBusReserve45         20      // int8_t
#define MOD_CHModBuscalculationValueDiff 21      // int32_t
#define MOD_CHModBusReserve47         21      // int8_t
#define MOD_CHModBusReserve48         21      // int8_t
#define MOD_CHModBusReserve49         21      // int8_t
#define MOD_CHModBusReserve50         21      // int8_t
#define MOD_CHModBuscalculationValueAdd 25      // int32_t
#define MOD_CHModBusReserve52         25      // int8_t
#define MOD_CHModBusReserve53         25      // int8_t
#define MOD_CHModBusReserve54         25      // int8_t
#define MOD_CHModBusReserve55         25      // int8_t
#define MOD_CHModBusSelectionVirtualZaehler1 26      // 1 Bit, Bit 7
#define     MOD_CHModBusSelectionVirtualZaehler1Mask 0x80
#define     MOD_CHModBusSelectionVirtualZaehler1Shift 7
#define MOD_CHModBusMathOperationVirtualZaehler1 27      // 8 Bits, Bit 7-0
#define MOD_CHModBusSelectionVirtualZaehler2 28      // 1 Bit, Bit 7
#define     MOD_CHModBusSelectionVirtualZaehler2Mask 0x80
#define     MOD_CHModBusSelectionVirtualZaehler2Shift 7
#define MOD_CHModBusMathOperationVirtualZaehler2 29      // 8 Bits, Bit 7-0
#define MOD_CHModBusSelectionVirtualZaehler3 30      // 1 Bit, Bit 7
#define     MOD_CHModBusSelectionVirtualZaehler3Mask 0x80
#define     MOD_CHModBusSelectionVirtualZaehler3Shift 7
#define MOD_CHModBusMathOperationVirtualZaehler3 31      // 8 Bits, Bit 7-0
#define MOD_CHModBusSelectionVirtualZaehler4 32      // 1 Bit, Bit 7
#define     MOD_CHModBusSelectionVirtualZaehler4Mask 0x80
#define     MOD_CHModBusSelectionVirtualZaehler4Shift 7
#define MOD_CHModBusMathOperationVirtualZaehler4 33      // 8 Bits, Bit 7-0
#define MOD_CHModBusTypZaehler1       34      // 8 Bits, Bit 7-0
#define MOD_CHModBusTypZaehler2       35      // 8 Bits, Bit 7-0
#define MOD_CHModBusTypZaehler3       36      // 8 Bits, Bit 7-0
#define MOD_CHModBusTypZaehler4       37      // 8 Bits, Bit 7-0

// Communication objects per channel (multiple occurance)
#define MOD_KoOffset 30
#define MOD_KoBlockSize 1
#define MOD_KoGO_BASE_ 0

// Communication objects with single occurance
#define MOD_KoHeartbeat 1
#define MOD_KoDebugModbus 2
#define MOD_KoDebugMBus 3
#define MOD_KoS01_ZaehlerWert 4
#define MOD_KoS02_ZaehlerWert 5
#define MOD_KoZaehler1_base 6
#define MOD_KoZaehler2_base 7
#define MOD_KoZaehler3_base 8
#define MOD_KoZaehler4_base 9
#define MOD_KoPower1_base 10
#define MOD_KoPower2_base 11
#define MOD_KoPower3_base 12
#define MOD_KoPower4_base 13
#define MOD_KoS01_Ges_Verbrauch 14
#define MOD_KoS01_Akt1_Verbrauch 15
#define MOD_KoS01_Akt2_Verbrauch 16
#define MOD_KoS01_Res 17
#define MOD_KoS02_Ges_Verbrauch 18
#define MOD_KoS02_Akt1_Verbrauch 19
#define MOD_KoS02_Akt2_Verbrauch 20
#define MOD_KoS02_Res 21

