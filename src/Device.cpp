#include <Arduino.h>

#include <knx.h>
#include "hardware.h"
#include "KnxHelper.h"
#include "ModbusGateway.h"
#include "ModbusRead.h"
#include "ModbusMaster.h"
#include "Modbus.h"
#include "wiring_private.h" // pinPeripheral() function
#include "S0Function.h"
#include "Device.h"
#include "LED_Statusanzeige.h"

uint32_t heartbeatDelay = 0;
uint32_t startupDelay = 0;

uint8_t usedModbusChannels = 0;

bool modbusReady = false;
bool S0Ready = false;

// Functions
void ProcessHeartbeat()
{
  // the first heartbeat is send directly after startup delay of the device
  if (heartbeatDelay == 0 || delayCheck(heartbeatDelay, knx.paramInt(MOD_Heartbeat) * 1000))
  {
    // we waited enough, let's send a heartbeat signal
    knx.getGroupObject(MOD_KoHeartbeat).value(true, getDPT(VAL_DPT_1));

    heartbeatDelay = millis();
  }
}

/**
 * @return upper border of used channels
 */
uint8_t getUsedModbusChannels()
{
  uint8_t countChannels = 0;
  for (int channel = 0; channel < ModbusChannel; channel++)
  {
    if (knx.paramByte(getPar(MOD_CHModbusSlaveSelection, channel)) != 0) // wenn ein Slave gewählt wurde
    {
      if (knx.paramByte(getPar(MOD_CHModBusDptSelection, channel)) != 0) // und zusätzlich auch ein DPT
      {
        countChannels = channel + 1;
      }
    }
  }
  return countChannels;
}

bool setupModbus()
{
#ifdef LED_YELLOW_PIN
  pinMode(MAX485_RE_NEG, OUTPUT);
#endif
  pinMode(MAX485_DE, OUTPUT);
// Init in receive mode
#ifdef LED_YELLOW_PIN
  digitalWrite(MAX485_RE_NEG, 0);
#endif
  digitalWrite(MAX485_DE, 0);

  modbusInitSerial();

  modbusInitSlaves();

#ifdef Serial_Debug_Modbus
  SERIAL_PORT.println("Modbus Setup Done");
#endif

  return true;
}

void setupS0()
{
  if (knx.paramByte(MOD_DefineS0zaehler1) > 0)
  {
    SERIAL_PORT.println("S01 = Aktiv");
    pinMode(S0_CH1, INPUT);
    attachInterrupt(digitalPinToInterrupt(S0_CH1), functionS01, FALLING);

    SERIAL_PORT.print("Impulse: ");
    SERIAL_PORT.println(setZaehlerImpulse(0, knx.paramWord(MOD_S01Impulse)));

#ifdef Serial_Debug_S0

    SERIAL_PORT.print("DefineUnit S02: ");
    SERIAL_PORT.print(knx.paramByte(MOD_DefineUnitS02));
    SERIAL_PORT.print(" ");
    SERIAL_PORT.println(knx.paramByte(MOD_DefineUnitS02), BIN);

    SERIAL_PORT.print("min Value S1: ");
    SERIAL_PORT.print(knx.paramWord(MOD_DefineMinValueS01));
    SERIAL_PORT.print(" ");
    SERIAL_PORT.println(knx.paramWord(MOD_DefineMinValueS01), BIN);

    SERIAL_PORT.print("min Value S2: ");
    SERIAL_PORT.print(knx.paramWord(MOD_DefineMinValueS02));
    SERIAL_PORT.print(" ");
    SERIAL_PORT.println(knx.paramWord(MOD_DefineMinValueS02), BIN);
#endif

#ifdef Debug_S0_LED
    pinMode(Diag_LED, OUTPUT);
    digitalWrite(Diag_LED, false);
#endif
  }

  if (knx.paramByte(MOD_DefineS0zaehler2) > 0)
  {
    SERIAL_PORT.println("S02 = Aktiv");
    pinMode(S0_CH2, INPUT);
    attachInterrupt(digitalPinToInterrupt(S0_CH2), functionS02, FALLING);

    SERIAL_PORT.print("Impulse: ");
    SERIAL_PORT.println(setZaehlerImpulse(1, knx.paramWord(MOD_S02Impulse)));
  }
}

// true solange der Start des gesamten Moduls verzögert werden soll
bool startupDelayfunc()
{
  if (knx.paramWord(MOD_StartupDelaySelection) == 0) // manuelle Eingabe
  {
    return !delayCheck(startupDelay, knx.paramInt(MOD_StartupDelay) * 1000);
  }
  else
  {
    return !delayCheck(startupDelay, knx.paramWord(MOD_StartupDelaySelection) * 1000);
  }
}

void ProcessKoCallback(GroupObject &iKo)
{
  // Compute modbus channel number
  int channel = (iKo.asap() - MOD_KoOffset - MOD_KoGO_BASE_) /  MOD_KoBlockSize;
  if (channel >= 0 && channel < ModbusChannel) {
#ifdef Serial_Debug_Modbus
    SERIAL_PORT.print("KO: ");
    SERIAL_PORT.println(channel + 1);
#endif
    // Get slave number
    int slave = knx.paramByte(getPar(MOD_CHModbusSlaveSelection, channel));
    if (slave >= 1 && slave < MaxCountSlaves) {
      Slave[slave-1].ReadyToSendModbus(channel);
    }
  }
}

uint8_t get_PROG_LED_PIN(uint8_t hwID)
{
  switch (hwID)
  {
  case 7: // V1.x
    return 9;
    break;
  case 6: // V2.x
    return 6;
    break;
  default:
    return 255;
    break;
  }
}

uint8_t get_PROG_BUTTON_PIN(uint8_t hwID)
{
  switch (hwID)
  {
  case 7: // V1.x
    return A1;
    break;
  case 6: // V2.x
    return 7;
    break;
  default:
    return 250;
    break;
  }
}

uint8_t get_PROG_LED_PIN_ACTIVE_ON(uint8_t hwID)
{
  switch (hwID)
  {
  case 7: // V1.x
    return A1;
    break;
  case 6: // V2.x
    return 7;
    break;
  default:
    return 251;
    break;
  }
}

uint8_t get_HW_ID()
{
  uint8_t hw_ID = 0;
  // Set Inputs
  pinMode(ID1, INPUT_PULLUP);
  pinMode(ID2, INPUT_PULLUP);
  pinMode(ID3, INPUT_PULLUP);
  // read Inputs
  bitWrite(hw_ID, 0, digitalRead(ID1));
  bitWrite(hw_ID, 1, digitalRead(ID2));
  bitWrite(hw_ID, 2, digitalRead(ID3));

  return hw_ID;
}

void initHW(uint8_t hwID)
{
  switch (hwID)
  {
  case 7:
    SERIAL_PORT.println("HW_ID: V1.x");
    break;
  case 6:
    SERIAL_PORT.println("HW_ID: V2.x");
    break;

  default:
    SERIAL_PORT.print("HW_ID: ERROR ");
    SERIAL_PORT.println(hwID);
    break;
  }

  //initI2cStatusLeds();
}

void appSetup()
{
  // Modbus
  modbusReady = setupModbus();
  // determine count of used channels
  usedModbusChannels = getUsedModbusChannels();
  SERIAL_PORT.print("Channel Count:");
  SERIAL_PORT.println(usedModbusChannels);

  // S0
  setupS0();

  if (knx.configured())
  {
    if (GroupObject::classCallback() == 0)
      GroupObject::classCallback(ProcessKoCallback);
  }
}

void appLoop()
{
  if (!knx.configured())
    return;

  // handle KNX stuff
  if (startupDelayfunc())
    return;

  ProcessHeartbeat();

  Process_S0(0); //CH = S01
  Process_S0(1); //CH = S02

  ModbusRead(usedModbusChannels);
}